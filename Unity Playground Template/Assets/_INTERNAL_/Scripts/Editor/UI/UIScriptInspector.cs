﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(UIScript))]
public class UIScriptInspector : InspectorBase
{
	private string explanation = "Use the UI to visualise points and health for the players.";
	private string lifeReminder = "Don't forget to use the script HealthSystemAttribute on the player(s)!";

	private int nOfPlayers = 0, gameType = 0;
	private string[] readablePlayerEnum = new string[]{"One player", "Two players"};
	private string[] readableGameTypesEnum = new string[]{"Score", "Life", "Endless"};
    
    private string sceneWarning = "WARNING: Make sure the scene is enabled in the Build Settings scenes list.";
    private string sceneInfo = "WARNING; To add a new level, save a Unity scene and then go to File > Build Settings... and add the scene to the list.";

    public override void OnInspectorGUI()
	{
		GUILayout.Space(10);
		EditorGUILayout.HelpBox(explanation, MessageType.Info);

		nOfPlayers = serializedObject.FindProperty("numberOfPlayers").intValue;
		gameType = serializedObject.FindProperty("gameType").intValue;

		nOfPlayers = EditorGUILayout.Popup("Number of players", nOfPlayers, readablePlayerEnum);

		gameType = EditorGUILayout.Popup("Game type", gameType, readableGameTypesEnum);
		if(gameType == 0) //score game
		{
			EditorGUILayout.PropertyField(serializedObject.FindProperty("scoreToWin"));
		}

		if(gameType == 1) //life
		{
			EditorGUILayout.HelpBox(lifeReminder, MessageType.Info);
		}

        EditorGUILayout.PropertyField(serializedObject.FindProperty("displayType"));
        UIScript.HealthDisplayType displayType = (UIScript.HealthDisplayType)serializedObject.FindProperty("displayType").intValue;
        switch (displayType)
        {
            case UIScript.HealthDisplayType.TEXT:
                break;
            case UIScript.HealthDisplayType.HEART:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("heartIcon"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("heartContainerIcon"));
                break;
            /*case UIScript.HealthDisplayType.BAR:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("healthBarFull"), true);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("healthBarContainer"), true);
                break;*/
        }


        // Scenes
        EditorGUILayout.PropertyField(serializedObject.FindProperty("endGameAction"));
        UIScript.EndGameAction endGameAction = (UIScript.EndGameAction)serializedObject.FindProperty("endGameAction").intValue;
        if (endGameAction == UIScript.EndGameAction.CHANGE_SCENE)
        {
            bool displayWarning = false;
            if (EditorBuildSettings.scenes.Length > 0)
            {
                int sceneIdWin = 0;
                int sceneIdLose = 0;
                string sceneNamePropertyWin = serializedObject.FindProperty("winScene").stringValue;
                string sceneNamePropertyLose = serializedObject.FindProperty("loseScene").stringValue;

                //get available scene names and clean the names
                string[] sceneNames = new string[EditorBuildSettings.scenes.Length + 1];
                sceneNames[0] = "RELOAD LEVEL";
                int i = 1;
                foreach (EditorBuildSettingsScene s in EditorBuildSettings.scenes)
                {
                    int lastSlash = s.path.LastIndexOf("/");
                    string shortPath = s.path.Substring(lastSlash + 1, s.path.Length - 7 - lastSlash);
                    sceneNames[i] = shortPath;

                    if (shortPath == sceneNamePropertyWin)
                    {
                        sceneIdWin = i;

                        if (!s.enabled)
                        {
                            displayWarning = true;
                        }
                    }
                    if (shortPath == sceneNamePropertyLose)
                    {
                        sceneIdLose = i;

                        if (!s.enabled)
                        {
                            displayWarning = true;
                        }
                    }

                    i++;
                }


                //Display the selector
                sceneIdWin = EditorGUILayout.Popup("Winning scene", sceneIdWin, sceneNames);
                sceneIdLose = EditorGUILayout.Popup("Losing scene", sceneIdLose, sceneNames);

                if (displayWarning)
                {
                    EditorGUILayout.HelpBox(sceneWarning, MessageType.Warning);
                }

                if (sceneIdWin == 0)
                {
                    serializedObject.FindProperty("winScene").stringValue = UIScript.SAME_SCENE; //this means same scene
                }
                else
                {
                    serializedObject.FindProperty("winScene").stringValue = sceneNames[sceneIdWin];
                }
                if (sceneIdLose == 0)
                {
                    serializedObject.FindProperty("loseScene").stringValue = UIScript.SAME_SCENE; //this means same scene
                }
                else
                {
                    serializedObject.FindProperty("loseScene").stringValue = sceneNames[sceneIdLose];
                }
            }
            else
            {
                EditorGUILayout.Popup("Winning scene", 0, new string[] { "No scenes available!" });
                EditorGUILayout.Popup("Losing scene", 0, new string[] { "No scenes available!" });
                EditorGUILayout.HelpBox(sceneInfo, MessageType.Warning);
            }
        }
        




        //write all the properties back
        serializedObject.FindProperty("gameType").intValue = gameType;
		serializedObject.FindProperty("numberOfPlayers").intValue = nOfPlayers;

		if(GUI.changed)
		{
			serializedObject.ApplyModifiedProperties();
		}
	}
}
