﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour
{
    public enum CountMode
    {
        COUNT_UP,
        COUNT_DOWN
    }

    public CountMode countMode = CountMode.COUNT_UP;

    [ShowOnEnum("countMode", (int)CountMode.COUNT_DOWN)]
    public int startingCount = 0;

    private Text text;
    private float timer;


    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        timer = startingCount;
        SetText();
    }

    // Update is called once per frame
    void Update()
    {
        if (countMode == CountMode.COUNT_UP)
            timer += Time.deltaTime;
        else if (countMode == CountMode.COUNT_DOWN)
            timer -= Time.deltaTime;

        SetText();
    }

    private void SetText()
    {
        if (text != null)
        {
            int clampedTime = (int)timer;
            text.text = clampedTime.ToString();
        }
    }
}
